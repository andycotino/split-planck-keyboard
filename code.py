from adafruit_hid.keyboard import Keyboard
from adafruit_hid.keycode import Keycode
import usb_hid
import keypad
import board
import time

rows    = (
        board.GP16, board.GP17, board.GP18, board.GP19
)

columns = (
        board.GP0, board.GP1, board.GP2, board.GP3,
        board.GP4, board.GP5, board.GP6, board.GP7,
        board.GP8, board.GP9, board.GP10, board.GP11
)

km = keypad.KeyMatrix(
    row_pins    = rows,
    column_pins = columns,
    columns_to_anodes = True
)

keys_pressed = [key_number for key_number in range(0, km.key_count)]

default_mapping = [Keycode.TAB, Keycode.Q, Keycode.W, Keycode.E, Keycode.R, Keycode.T, Keycode.Y, Keycode.U, Keycode.I, Keycode.O, Keycode.P, Keycode.BACKSPACE,
                   Keycode.ESCAPE, Keycode.A, Keycode.S, Keycode.D, Keycode.F, Keycode.G, Keycode.H, Keycode.J, Keycode.K, Keycode.L, Keycode.SEMICOLON, Keycode.QUOTE,
                   Keycode.SHIFT, Keycode.Z, Keycode.X, Keycode.C, Keycode.V, Keycode.B, Keycode.N, Keycode.M, Keycode.COMMA, Keycode.PERIOD, Keycode.FORWARD_SLASH, Keycode.ENTER,
                   Keycode.CONTROL, Keycode.ALT, Keycode.COMMAND, Keycode.DELETE, 'lower', Keycode.SPACE, Keycode.SPACE, 'upper', Keycode.LEFT_ARROW, Keycode.DOWN_ARROW, Keycode.UP_ARROW, Keycode.RIGHT_ARROW]

upper_mapping = [Keycode.GRAVE_ACCENT, Keycode.ONE, Keycode.TWO, Keycode.THREE, Keycode.FOUR, Keycode.FIVE, Keycode.SIX, Keycode.SEVEN, Keycode.EIGHT, Keycode.NINE, Keycode.ZERO, Keycode.BACKSPACE,
                   Keycode.ESCAPE, Keycode.F1, Keycode.F2, Keycode.F3, Keycode.F4, Keycode.F5, Keycode.F6, Keycode.MINUS, Keycode.EQUALS, Keycode.LEFT_BRACKET, Keycode.RIGHT_BRACKET, Keycode.BACKSLASH,
                   Keycode.SHIFT, Keycode.F7, Keycode.F8, Keycode.F9, Keycode.F10, Keycode.F11, Keycode.F12, Keycode.POUND, Keycode.FORWARD_SLASH, Keycode.PAGE_UP, Keycode.PAGE_DOWN, Keycode.ENTER,
                   Keycode.CONTROL, Keycode.ALT, Keycode.COMMAND, Keycode.DELETE, 'lower', Keycode.SPACE, Keycode.SPACE, 'upper', Keycode.LEFT_ARROW, Keycode.DOWN_ARROW, Keycode.UP_ARROW, Keycode.RIGHT_ARROW]

lower_mapping = ['shift', 'shift', 'shift', 'shift', 'shift', 'shift', 'shift', 'shift', 'shift', 'shift', 'shift', Keycode.BACKSPACE,
                   Keycode.ESCAPE, Keycode.F1, Keycode.F2, Keycode.F3, Keycode.F4, Keycode.F5, Keycode.F6, 'shift', 'shift', 'shift', 'shift', 'shift',
                   Keycode.SHIFT, Keycode.F7, Keycode.F8, Keycode.F9, Keycode.F10, Keycode.F11, Keycode.F12, Keycode.POUND, Keycode.FORWARD_SLASH, Keycode.HOME, Keycode.END, Keycode.ENTER,
                   Keycode.CONTROL, Keycode.ALT, Keycode.COMMAND, Keycode.DELETE, 'lower', Keycode.SPACE, Keycode.SPACE, 'upper', Keycode.LEFT_ARROW, Keycode.DOWN_ARROW, Keycode.UP_ARROW, Keycode.RIGHT_ARROW]

mapping = {}

for default_key_value, upper_key_value, lower_key_value, key_number in zip(default_mapping, upper_mapping, lower_mapping, range(0, km.key_count)):
    mapping[key_number] = {
        'default' : {
            'key_value' : default_key_value
        },
        'upper' : {
            'key_value' : upper_key_value
        },
        'lower' : {
            'key_value' : lower_key_value
        },
        'pressed' : False
    }
    
    
# Set up a keyboard device.
kbd = Keyboard(usb_hid.devices)
shift = 24
control = 36

layers = ('lower', 'upper')
layer = 'default'  
while True:
    try:
        event = km.events.get()

        if event:
            # Find the value of the key pressed given the current active layer
            key_pressed = mapping[event.key_number][layer]['key_value']
                        
            # Some garbage edge case to pass the codes for the upper layer
            if layer == 'lower' and key_pressed == 'shift' and event.pressed:
                kbd.send(Keycode.SHIFT, mapping[event.key_number]['upper']['key_value'])
                mapping[event.key_number]['pressed'] = True
            # Activating an alternate layer
            elif key_pressed in layers and event.pressed:
                layer = key_pressed
                pass
            # Deactivating a layer back to the default layer
            elif key_pressed in layers and event.released:
                layer = 'default'
                pass
            # Pressing a regular key
            elif event.pressed:
                kbd.press(key_pressed)
                mapping[event.key_number]['pressed'] = True
            # Releasing a regular key
            else:
                kbd.release(key_pressed)
                mapping[event.key_number]['pressed'] = False
                
    except:
        # Ohh well
        pass
